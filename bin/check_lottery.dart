import 'dart:io';

import 'package:check_lottery/check_lottery.dart' as check_lottery;

void main() {
    printEnterNum();
    var number = stdin.readLineSync();
    check_lot(number);
}

void printEnterNum() {
  print("Enter your lottery number");
}

void check_lot (var number){
  if (number.length != 6){
    print("Wrong number please check your ticket!!!");
    return;
  }
  if (number == "436594"){
    print("You got 1st prize");
    return;
  }
  else{
    check_lot_frontNum(number);
  }
}

void check_lot_frontNum(var number) {
  if (number.substring(0,3) == "266" || number.substring(0,3) == "893"){
    print("You got three front number prize");
    return;
  }
  else{
    check_lot_backNum(number);
  }
}

void check_lot_backNum(var number) {
  if (number.substring(number.length-3) == "447" || number.substring(number.length-3) == "282"){
    print("You got three back number prize");
    return;
  }
  else{
    check_lot_2backNum(number);
  }
}

void check_lot_2backNum(number) {
  if (number.substring(number.length-2) == "14"){
    print("You got two back number prize");
    return;
  }
  else{
    print("Unlucky, maybe next times");
  }
}